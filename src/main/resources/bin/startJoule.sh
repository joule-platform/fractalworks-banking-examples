#!/bin/bash
#
# Copyright 2020-present FractalWorks Ltd.
#
# Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
source ${PWD}/version.env

ENV_FILE=${PWD}/bin/app.env
if test -f "$ENV_FILE"; then
    echo "Joule Version: ${JOULE_VERSION}"
    echo "Applying environment file. ${PWD}/bin/app.env"
    source ${PWD}/bin/app.env
fi

if [ -z "${JOULE_HOST_NAME}" ]; then
  JOULE_HOST_NAME="localhost"
fi

if [ -z "${JOULE_JMX_PORT}" ]; then
  JOULE_JMX_PORT=1098
fi

if [ -z "${JOULE_MEMORY}" ]; then
  JOULE_MEMORY=-Xmx8G
fi

export VERSION=${JOULE_VERSION}
export JOULE_HOME=${PWD}
export LIBS=${JOULE_HOME}/lib
export USERLIBS=${JOULE_HOME}/userlibs
export LOG4J_PATH=${JOULE_HOME}/META-INF/joule/log4j2.xml
export CLASSPATH=$(find ${LIBS} -name '*.jar' | xargs echo | tr ' ' ':'):$(find ${USERLIBS} -name '*.jar' | xargs echo | tr ' ' ':')
export JVM_SWITCHES="-ea --add-opens java.base/java.lang=ALL-UNNAMED"

mkdir -p ${PWD}/logs
mkdir -p ${PWD}/data

java=java
if test -n "$JAVA_HOME"; then
    java="$JAVA_HOME/bin/java"
fi

sourcefile=${PWD}/$SOURCEFILE
enginefile=${PWD}/$ENGINEFILE
publishfile=${PWD}/$PUBLISHFILE
refdatafile=${PWD}/$REFERENCEDATA

JOULE_CONFIG_PATH=${JOULE_HOME}/conf/joule.properties

usage()
{
    echo "usage: startJoule [[[-s sourcefile ] [-u usecase] [-p publishfile] [-r refdatafile] [-m jvmmemory]] | [-h]]"
}

while [ "$1" != "" ]; do
    case $1 in
        -s | --sourcefile )     shift
                                sourcefile=$1
                                ;;
        -r | --refdatafile )    shift
                                refdatafile=$1
                                ;;
        -u | --usecase )     shift
                                enginefile=$1
                                ;;
        -p | --publishfile )    shift
                                publishfile=$1
                                ;;
        -m | --jvmmemory )      shift
                                MEM=$1
                                ;;
        -h | --help )         usage
                              exit
                              ;;
        * )                   usage
                              exit 1
    esac
    shift
done

if [ ! -f "$sourcefile" ]; then
  echo "Source file does not exists. Exiting"
  exit 1
fi

if [ ! -f "$enginefile" ]; then
  echo "Engine file does not exists. Exiting"
  exit 1
fi

if [ ! -f "$publishfile" ]; then
  echo "Publish file does not exists. Exiting"
  exit 1
fi

java=java
if test -n "$JAVA_HOME"; then
    java="$JAVA_HOME/bin/java"
fi

JMX_CONFIG="-Dcom.sun.management.jmxremote.port=${JOULE_JMX_PORT} -Dcom.sun.management.jmxremote.rmi.port=${JOULE_JMX_PORT} -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Djava.rmi.server.hostname=${JOULE_HOST_NAME} -Dcom.sun.management.jmxremote.local.only=false"
JVM_DEBUG="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005"

echo "Starting Joule"
if [ -f "$refdatafile" ]; then
  nohup java ${JVM_SWITCHES} ${JMX_CONFIG} ${JVM_DEBUG} -Dlog4j.configuration=file://${LOG4J_PATH} ${JOULE_MEMORY} -XX:+UseG1GC -Dpolyglot.engine.WarnInterpreterOnly=false -cp ${CLASSPATH}  com.fractalworks.streams.joule.JouleCLI -c ${JOULE_CONFIG_PATH} -s ${SOURCEFILE} -r ${REFERENCEDATA}  -u ${ENGINEFILE} -p ${PUBLISHFILE} &
else
  nohup java ${JVM_SWITCHES} ${JMX_CONFIG} ${JVM_DEBUG} -Dlog4j.configuration=file://${LOG4J_PATH} ${JOULE_MEMORY} -XX:+UseG1GC -Dpolyglot.engine.WarnInterpreterOnly=false -cp ${CLASSPATH} com.fractalworks.streams.joule.JouleCLI -c ${JOULE_CONFIG_PATH} -s ${SOURCEFILE} -u ${ENGINEFILE} -p ${PUBLISHFILE} &
fi

_pid=$!

echo "$_pid" > ${PWD}/logs/joule.pid
echo "ProcessId" "$_pid"

exit 1
