package com.fractalworks.examples.banking.data;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.TranslationException;
import com.fractalworks.streams.sdk.codec.CustomTransformer;

import java.util.ArrayList;
import java.util.Collection;

public class StockAnalyticRecordTransform implements CustomTransformer<StockAnalyticRecord> {
    @Override
    public Collection<StockAnalyticRecord> transform(Collection<StreamEvent> collection) throws TranslationException {
        Collection<StockAnalyticRecord> analytics = new ArrayList<>();
        if( collection!= null) {
            for(StreamEvent e : collection){
                analytics.add(transform(e));
            }
        }
        return analytics;
    }

    @Override
    public StockAnalyticRecord transform(StreamEvent streamEvent) throws TranslationException {
        var time = (streamEvent.getValue("time")!=null) ? (Long)streamEvent.getValue("time") : System.currentTimeMillis();
        var askFirst = (streamEvent.getValue("ask_FIRST")!=null) ? (Double)streamEvent.getValue("ask_FIRST") : Double.NaN;
        var askLast = (streamEvent.getValue("ask_LAST")!=null) ? (Double)streamEvent.getValue("ask_LAST") : Double.NaN;

        return new StockAnalyticRecord(
                (String)streamEvent.getValue("symbol"),
                time,
                askFirst,
                askLast);
    }
}
